FROM php:7.4-fpm

WORKDIR /root/

RUN apt-get update \
    && apt install -y nano \
    && apt install -y htop \
    && apt install -y iputils-ping \
    && apt install -y telnet \
    && apt install -y wget \
    && wget https://wordpress.org/wordpress-4.8.19.tar.gz \
    && tar xvaf wordpress-4.8.19.tar.gz \
    && apt-get install -y --no-install-recommends \
			   libpng-dev \
			   libzip-dev \
			   libicu-dev \
			   libzip4 \
	&& pecl install xdebug \
	&& docker-php-ext-install opcache \
        && docker-php-ext-enable xdebug \
	&& docker-php-ext-install pdo_mysql \
	&& docker-php-ext-install exif \
	&& docker-php-ext-install zip \
	&& docker-php-ext-install gd \
	&& docker-php-ext-install intl \
	&& docker-php-ext-install mysqli 
    

# Clear cache
RUN apt-get clean && rm -rf /var/lib/apt/lists/*

#ADD ./php.ini-production /usr/local/etc/php/php.ini

COPY ./wp-config.php /root/wordpress/wp-config.php

RUN cp /usr/local/etc/php/php.ini-production /usr/local/etc/php/php.ini

RUN echo "date.timezone=Asia/Ho_Chi_Minh" >> /usr/local/etc/php/php.ini

EXPOSE 9000

VOLUME [ "/var/www/html" ]

ENTRYPOINT [ "docker-php-entrypoint" ]

CMD [ "php-fpm" ]
